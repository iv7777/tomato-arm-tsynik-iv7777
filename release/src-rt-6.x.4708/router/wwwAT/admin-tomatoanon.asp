<!--
Tomato GUI
Copyright (C) 2012 Shibby
http://openlinksys.info
For use with Tomato Firmware only.
No part of this file may be used without permission.
--><title><% translate("TomatoAnon Project"); %></title>
<content>
	<script type="text/javascript">
		//	<% nvram("tomatoanon_enable,tomatoanon_answer,tomatoanon_id,tomatoanon_notify"); %>
		$('.anonlink').append('<a title=<% translate("Checkout my router"); %> class="pull-right" href="http://tomato.groov.pl/tomatoanon.php?search=9&routerid=<% nv('tomatoanon_id'); %>" target="_blank"><i class="icon-forward"></i></a>');
		function verifyFields(focused, quiet)
		{
			var o = (E('_tomatoanon_answer').value == '1');
			E('_tomatoanon_enable').disabled = !o;
			var s = (E('_tomatoanon_enable').value == '1');
			E('_f_tomatoanon_notify').disabled = !o || !s;
			return 1;
		}
		function save()
		{
			if (verifyFields(null, 0)==0) return;
			var fom = E('_fom');
			fom.tomatoanon_notify.value = E('_f_tomatoanon_notify').checked ? 1 : 0;
			fom._service.value = 'tomatoanon-restart';
			form.submit('_fom', 1);
		}
		function init()
		{
			var anon = true;
		}

		function submit_complete()
		{
			document.location.reload();
		}
	</script>


	<form id="_fom" method="post" action="tomato.cgi">
		<input type="hidden" name="_nextpage" value="/#admin-tomatoanon.asp">
		<input type="hidden" name="_service" value="tomatoanon-restart">
		<input type='hidden' name='tomatoanon_notify'>

		<div class="box main">
			<div class="heading"><% translate("TomatoAnon Project"); %></div>
			<div class="content">
				<p>
					<% translate("I would like to present you with a new project I've been working on, called TomatoAnon"); %>.<br/>
					<% translate("The TomatoAnon script will send (to a database) information about your router's model and installed version of Tomato"); %>.<br/>
					<% translate("The information submitted is 100% anonymous and will ONLY be used for statistical purposes"); %>.<br/>
					<b><% translate("This script does NOT send any private or personal information whatsoever (like MAC`s, IP`s etc)"); %>!</b><br/>
					<% translate("Script is fully open, and written in bash. Anyone is free to look at the content that is submitted to the database"); %>.<br/>
				</p>

				<p>
					<% translate("The submitted results can be viewed on the"); %> <a href=http://tomato.groov.pl/tomatoanon.php target=_blanc><b>http://tomato.groov.pl/tomatoanon.php</b></a> <% translate("page"); %>.<br/>
					<% translate("This information may help you when choosing the best and most popular router available in your country"); %>.<br/>
					<% translate("You can check which version of Tomato is most commonly used and which one is the most stable"); %>.<br/>
					<% translate("If you don't agree with this script, or do not wish to use it, you can simply disable it"); %>.<br/>
					<% translate("You can always re-enable it at any time"); %>.<br/>
				</p>

				<p><% translate("The following data is sent by TomatoAnon"); %>:</p>
				<ul>
					<li><% translate("MD5SUM of WAN+LAN MAC addresses"); %> - <% translate("this will identify a router. Ex"); %>: 1c1dbd4202d794251ec1acf1211bb2c8</li>
					<li><% translate("Model of router. Ex"); %>: Asus RT-N66U</li>
					<li><% translate("Installed version of Tomato. Ex"); %>: 102 K26 USB</li>
					<li><% translate("Builtype. Ex"); %>: Mega-VPN-64K</li>
					<li><% translate("Uptime of your router. Ex: 3 days"); %></li>
					<li><% translate("That`s it"); %>!</li>
				</ul>

				<p><% translate("Thank you for reading and please make the right choice to help this project"); %>.</p>
				<p>	
				<b><% translate("Best Regards"); %>!</b>
				<p>
				<br/>
				<h3><% translate("Tomato Update Notification System"); %></h3>
				<br/>
				<script type="text/javascript">
					$('.box.main .content').forms([
						{ title: '<% translate("Enable"); %>', name: 'f_tomatoanon_notify', type: 'checkbox', value: nvram.tomatoanon_notify == '1' }
					]);
				</script>
					<b><% translate("When new tomato version will be available, you will be notified about this on"); %> <% translate("Status"); %> - <% translate("Overview"); %>.	</b><br/>
			</div>
		</div>

		<div class="box anon">
			<div class="heading anonlink"><% translate("TomatoAnon Settings"); %></div>
			<div class="content"></div>
			<script type="text/javascript">
				$('.box.anon .content').forms([
				{ title: '<% translate("Do you know what TomatoAnon doing"); %>?', name: 'tomatoanon_answer', type: 'select', options: [ ['0','<% translate("No, i don`t. Have to read all information, before i will make a choice"); %>'], ['1','<% translate("Yes, i do and want to make a choice"); %>'] ], value: nvram.tomatoanon_answer, suffix: ' '},
				{ title: '<% translate("Do you want enable TomatoAnon"); %>?', name: 'tomatoanon_enable', type: 'select', options: [ ['-1','<% translate("I`m not sure right now"); %>'], ['1','<% translate("Yes, i`m sure i do"); %>'], ['0','<% translate("No, i definitely wont enable it"); %>'] ], value: nvram.tomatoanon_enable, suffix: ' '}
				]);
			</script>
		</div>

		<button type="button" value="<% translate("Save"); %>" id="save-button" onclick="save()" class="btn btn-primary"><% translate("Save"); %> <i class="icon-check"></i></button>
		<button type="button" value="<% translate("Cancel"); %>" id="cancel-button" onclick="javascript:reloadPage();" class="btn"><% translate("Cancel"); %> <i class="icon-cancel"></i></button>
		<span id="footer-msg" class="alert alert-warning" style="visibility: hidden;"></span>

	</form>

	<script type="text/javascript">verifyFields(null, 1); init();</script>
</content>
